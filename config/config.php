<?php
/**
 * 总配置文件
 */

// 加载环境变量文件
require_once 'env.php';

// 加载数据库配置文件
$_configs = require_once 'db.' . ENV . '.php';

// 数据库库初始化
// 这里不考虑效率,将DB单例全部创建
foreach ($_configs['dbInfo'] as $key => $dbInfo) {
    if ($dbInfo['enable']) {
        $db = DB::init($_configs['dbInfo'], $key);
    }
}
