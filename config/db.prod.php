<?php
/**
 * 数据库设置（生产环境）
 */

// 数据库设置 - 可以设置多个数据库
$_configs['dbInfo']['phporm'] = array(
    'enable' => true,
    'dbType' => 'mysqli',
    'dbHost' => '127.0.0.1',
    'dbPort' => 3306,
    'dbName' => 'phporm',
    'dbUser' => 'root',
    'dbPass' => '123456',
    'charset' => 'utf8mb4'
);

// 高可用设置
$_configs['dbha'] = array('127.0.0.1');

return $_configs;