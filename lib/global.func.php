<?php

/**
 *  全局函数文件
 *
 *
 *
 * @version $Id: global.func.php 1752 2008-07-14 09:35:20Z $
 * @package common
 */


function autoload($className)
{
    if (class_exists($className, false) || interface_exists($className, false)) {
        return false;
    }
    // 拆解className
    $names = explode('_', $className);
    // 是否存在lib中
    if (is_file(ROOT_PATH . 'lib/' . implode(DIRECTORY_SEPARATOR, $names) . '.class.php')) {
        require_once ROOT_PATH . 'lib/' . implode(DIRECTORY_SEPARATOR, $names) . '.class.php';
    } else { // 否则为模块库文件
        $num = count($names);
        for ($i = 0; $i < $num; $i++) {
            // 如果模块长度和名称长度一样
            if ($num == $i + 1) {
                $fullPath = ROOT_PATH . strtolower(implode(DIRECTORY_SEPARATOR, $names)) . "/class/" . $names [$i] . '.class.php';
                $incPath = ROOT_PATH . strtolower(implode(DIRECTORY_SEPARATOR, $names)) . "/inc/config.inc.php";
                if (is_file($fullPath)) {
                    if (is_file($incPath)) {
                        require_once($incPath);
                    }

                    require_once $fullPath;
                    return true;
                }
            } else { // 否则前面作为模块名，后面作为类名
                $modulePath = array();
                $classPath = array();
                for ($j = 0; $j < $num; $j++) {
                    if ($j > $i) {
                        $classPath [] = $names [$j];
                    } else {
                        $modulePath [] = $names [$j];
                    }
                }
                $fullPath = ROOT_PATH . strtolower(implode(DIRECTORY_SEPARATOR, $modulePath)) . "/class/" . implode(DIRECTORY_SEPARATOR, $classPath) . '.class.php';
                $incPath = ROOT_PATH . strtolower(implode(DIRECTORY_SEPARATOR, $modulePath)) . "/inc/config.inc.php";
                if (is_file($fullPath)) {
                    if (is_file($incPath)) {
                        require_once($incPath);
                    }
                    require_once $fullPath;
                    return true;
                }
            }
        }
    }
}