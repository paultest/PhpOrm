<?php
/**
 * 数据库类 - Oracle类
 */

require_once dirname(__FILE__) . '/Exception.class.php';

class DB_Oracle extends DB
{
    protected $limit;
    protected $autoCommit = OCI_COMMIT_ON_SUCCESS;

    public function __construct(&$dbInfo, $dbKey, $fetchMode)
    {
        $this->dbKey = $dbKey;
        $this->dsn = &$dbInfo;
        $this->fecthMode = $fetchMode;
    }

    /**
     * 连接数据库
     *
     * 连接数据库之前可能需要改变DSN，一般不建议使用此方法
     *
     * @param string $type 选择连接主服务器或者从服务器
     * @return boolean
     * @throws DB_Exception
     */
    public function connect($type = 'slave')
    {
        if ($type == 'master' || !isset($this->dsn['slave'])) {
            $dbName = isset($this->dsn['master']) ? $this->dsn['master']['dbName'] : $this->dsn['dbName'];
            $dbUser = isset($this->dsn['master']) ? $this->dsn['master']['dbUser'] : $this->dsn['dbUser'];
            $dbPass = isset($this->dsn['master']) ? $this->dsn['master']['dbPass'] : $this->dsn['dbPass'];
            $this->uConn = oci_new_connect($dbUser, $dbPass, $dbName, str_replace('-', '', DEFAULT_CHARSET));
            if (!$this->uConn) {
                throw new DB_Exception('更新数据库连接失败');
            }
            if (!isset($this->dsn['slave'])) {
                $this->qConn = &$this->uConn;
            }
        } else {
            if (empty($this->dsn['slave'])) {
                $this->connect('master');
                return $this->qConn = &$this->uConn;
            }
            if (empty($_COOKIE[COOKIE_PREFIX . $this->dbKey . 'DbNo'])) {
                $dbNo = array_rand($this->dsn['slave']);
                setcookie(COOKIE_PREFIX . $this->dbKey . 'DbNo', $dbNo, null, COOKIE_PATH, COOKIE_DOMAIN);
            } else {
                $dbNo = $_COOKIE[COOKIE_PREFIX . $this->dbKey . 'DbNo'];
            }
            $dbInfo = $this->dsn['slave'][$dbNo];
            $dbName = $dbInfo['dbName'];
            $dbUser = $dbInfo['dbUser'];
            $dbPass = $dbInfo['dbPass'];
            $this->qConn = oci_new_connect($dbUser, $dbPass, $dbName, str_replace('-', '', DEFAULT_CHARSET));

            if (!$this->qConn) {
                if (!$this->uConn) {
                    $this->connect('slave');
                }
                $this->qConn = &$this->uConn;
                if (!$this->qConn) {
                    throw new DB_Exception('查询数据库选择失败');
                }
            }
        }
        return true;
    }

    /**
     * 关闭数据库连接
     *
     * 一般不需要调用此方法
     */
    public function close()
    {
        if (is_resource($this->uConn)) {
            oci_close($this->uConn);
        }
        if (is_resource($this->qConn)) {
            oci_close($this->qConn);
        }
    }

    /**
     * 执行一个SQL查询
     *
     * 本函数仅限于执行SELECT类型的SQL语句
     *
     * @param string $sql SQL查询语句
     * @param mixed $limit 整型或者字符串类型，如10|10,10
     * @param boolean $quick 是否快速查询
     * @return bool|mysqli_result 返回查询结果资源句柄
     * @throws DB_Exception
     */
    public function query($sql, $limit = null, $quick = false)
    {
        if ($limit != null) {
            $limit = explode(',', $limit);
            foreach ($limit as $key => $value) {
                $limit[$key] = (int)trim($value);
            }
            if (count($limit) == 1) {
                $limit[1] = $limit[0];
                $limit[0] = 0;
            }
        } else {
            $limit[0] = 0;
            $limit[1] = -1;
        }

        $this->sqls[] = $sql;
        $this->qSqls[] = $sql;
        $this->sql = $sql;
        $this->limit = $limit;
        $this->time[count($this->sqls) - 1][] = microtime(true);
        if (!$this->qConn) {
            $this->connect('slave');
        }
        $this->time[count($this->sqls) - 1][] = microtime(true);
        if (!$this->qrs = oci_parse($this->qConn, $sql)) {
            $e = oci_error($this->qrs);
            throw new DB_Exception('SQL解析失败:' . $e['message']);
        }

        if (!oci_execute($this->qrs, $this->autoCommit)) {
            $e = oci_error($this->qrs);
            throw new DB_Exception('查询执行失败:' . $e['message']);
        }
        $this->time[count($this->sqls) - 1][] = microtime(true);
        $this->queryNum++;
        return $this->qrs;
    }

    /**
     * 获取结果集
     *
     * 这里没有实现LIMIT,使用LIMIT请用getAll
     *
     * @param $rs
     * @param int $fetchMode
     * @return array 返回数据集每一行，并将$rs指针下移
     */
    public function fetch($rs, $fetchMode = self::DB_FETCH_DEFAULT)
    {
        switch ($fetchMode) {
            case 1 :
                $fetchMode = self::DB_FETCH_ASSOC;
                break;
            case 2 :
                $fetchMode = self::DB_FETCH_ROW;
                break;
            case 3 :
                $fetchMode = self::DB_FETCH_ARRAY;
                break;
            default :
                $fetchMode = self::DB_FETCH_DEFAULT;
                break;
        }
        $_record = oci_fetch_array($rs, $fetchMode);
        if (is_array($_record)) {
            foreach ($_record as $key => $value) {
                $key = strtolower($key);
                $key = preg_replace('/(?<=\w)_(\w)/e', 'strtoupper("\1")', $key);
                $record[$key] = $value;
            }
        }
        return $record;
    }

    /**
     * 执行一个SQL更新
     *
     * 本方法仅限数据库UPDATE操作
     *
     * @param string $sql 数据库更新SQL语句
     * @return boolean
     * @throws DB_Exception
     */
    public function update($sql)
    {
        $this->sql = $sql;
        $this->sqls[] = $this->sql;
        $this->uSqls[] = $this->sql;
        if (!$this->uConn) {
            $this->connect('master');
        }

        if (!$this->urs = oci_parse($this->uConn, $sql)) {
            $e = oci_error($this->urs);
            throw new DB_Exception('SQL解析失败:' . $e['message']);
        }

        if (!oci_execute($this->urs, $this->autoCommit)) {
            $e = oci_error($this->urs);
            throw new DB_Exception('更新失败:' . $e['message']);
        } else {
            $this->updateNum++;
            return $this->urs;
        }
    }

    /**
     * 返回SQL语句执行结果集中的第一行第一列数据
     *
     * @param string $sql 需要执行的SQL语句
     * @return mixed 查询结果
     * @throws DB_Exception
     */
    public function getOne($sql)
    {
        if (!$rs = $this->query($sql, 1, true)) {
            return false;
        }
        $result = array();
        oci_fetch_all($rs, $result, $this->limit[0], $this->limit[1], self::DB_FETCH_ROW);
        $this->free();
        return $result[0][0];
    }

    /**
     * 返回SQL语句执行结果集中的第一列数据
     *
     * @param string $sql 需要执行的SQL语句
     * @param mixed $limit 整型或者字符串类型，如10|10,10
     * @return bool|array 结果集数组
     * @throws DB_Exception
     */
    public function getCol($sql, $limit = null)
    {
        if (!$rs = $this->query($sql, $limit, true)) {
            return false;
        }
        $result = array();
        oci_fetch_all($rs, $result, $this->limit[0], $this->limit[1], self::DB_FETCH_ROW);
        foreach ($result as $key => $value) {
            $this->free();
            return $value;
        }
    }

    /**
     * 返回SQL语句执行结果中的第一行数据
     *
     * @param string $sql 需要执行的SQL语句
     * @param int $fetchMode 返回的数据格式
     * @return bool|array
     * @throws DB_Exception
     */
    public function getRow($sql, $fetchMode = self::DB_FETCH_DEFAULT)
    {
        if (!$rs = $this->query($sql, 1, true)) {
            return false;
        }
        oci_fetch_all($rs, $result, $this->limit[0], $this->limit[1], $fetchMode);
        $result = $this->changeData($result);
        $this->free();
        return $result[0];
    }

    /**
     * 返回SQL语句执行结果中的所有行数据
     *
     * @param string $sql 需要执行的SQL语句
     * @param mixed $limit 整型或者字符串类型，如10|10,10
     * @param int $fetchMode 返回的数据格式
     * @return bool|array 结果集二维数组
     * @throws DB_Exception
     */
    public function getAll($sql, $limit = null, $fetchMode = self::DB_FETCH_DEFAULT)
    {
        if (!$rs = $this->query($sql, $limit, true)) {
            return false;
        }
        $result = array();
        oci_fetch_all($rs, $result, $this->limit[0], $this->limit[1], $fetchMode);
        $result = $this->changeData($result);
        $this->free();
        return $result;
    }

    public function changeData($data)
    {
        $result = array();
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    // 将Oracle以下划线作为分割符的字段名转换为驼峰式
                    $key = strtolower($key);
                    $key = preg_replace('/(?<=\w)_(\w)/e', 'strtoupper("\1")', $key);
                    foreach ($value as $k => $v) {
                        $result[$k][$key] = $v;
                    }
                }
            }
        }
        return $result;
    }

    public function rows()
    {
        return oci_num_rows($this->qrs);
    }

    public function free()
    {
        if ($this->qrs) {
            oci_free_statement($this->qrs);
        }
        if ($this->urs) {
            oci_free_statement($this->urs);
        }
        $this->qrs = null;
        $this->urs = null;
    }

    public function escape($str)
    {
        if (is_array($str)) {
            foreach ($str as $key => $value) {
                $str[$key] = $this->escape($value);
            }
        } else {
            return str_replace("'", "''", $str);
        }
        return $str;
    }

    /**
     * 设置是否开启事务(是否自动提交)
     *
     * 当设置为false的时候,即开启事务处理模式,表类型应该为INNODB
     *
     * @param boolean $mode
     * @return void
     */
    public function autoCommit($mode = false)
    {
        if ($mode) {
            $this->autoCommit = OCI_COMMIT_ON_SUCCESS;
        } else {
            $this->autoCommit = OCI_DEFAULT;
        }
    }

    /**
     * 提交执行的SQL
     *
     * 当开启事务处理后,要手动提交执行的SQL语句
     *
     * @return boolean
     */
    public function commit()
    {
        return oci_commit($this->uConn);
    }

    /**
     * 回滚
     *
     * 当开启事务处理后,有需要的时候进行回滚
     *
     * @return boolean
     */
    public function rollback()
    {
        return oci_rollback($this->uConn);
    }

    public function __destruct()
    {
    }
}
