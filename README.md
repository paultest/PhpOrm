# PhpOrm

### 一、介绍
用PHP实现了一个ORM，能够实现：
* 支持MySQL、Oracle、PostgreSQL
* 新增/批量新增
* 查询单条/查询所有/条件查询/组合查询
* 缓存字段
* 条件查询支持：!=、like、in、not in、between、lt、gt、is_null等
* 排序
* limit
* 更新/批量更新
* 删除/批量删除

<br>

### 二、使用说明

更多的实例可以见test目录下的测试文件

<br>

#### 1. 实例化
注意：需要加载common.php文件
```php
<?php
require_once 'common.php';

try {
    // 数据库名.表名
    $table_name = 'phporm.app_info';

    // 实例化
    $model = PHPOrm::init($table_name);

} catch (\DB_Exception $e) {
    var_dump('数据库错误');
    var_dump($e->getMessage());
} catch (\Exception $e) {
    var_dump('其他错误');
    var_dump($e->getMessage());
}
```

<br>

#### 2. 新增
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);
    
// 新增一条数据
$single_data = [
    'name' => '第53个应用',
    'description' => '这个是描述',
    'product_id' => 4,
    'service_id' => 3,
    'developer' => 'hello',
    'operater' => 'xiaoming',
    'ipmortance' => 4,
    'app_type' => 'web',
];
$single_res = $model->insert($single_data);
var_dump($single_res);

// 新增一条数据
$batch_data = [
    [
        'name' => '第54个应用',
        'description' => '这个是描述',
        'product_id' => 4,
        'service_id' => 3,
        'developer' => 'hello',
        'operater' => 'xiaoming',
        'ipmortance' => 4,
        'app_type' => 'web',
    ],
    [
        'name' => '第55个应用',
        'description' => '这个是描述',
        'product_id' => 3,
        'service_id' => 6,
        'developer' => 'hello2',
        'operater' => 'xiaoming55',
        'ipmortance' => 4,
        'app_type' => 'server',
    ],
];
$batch_res = $model->batch_insert($batch_data);
var_dump($batch_res);
```

<br>

#### 3. 更新
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$update_data = [
    'name' => '第54个应用 - 新的',
    'description' => '这个是描述 - 新的'
];
$update_res = $model->update($update_data, ['id' => 57]);
var_dump($update_res);
```

<br>

#### 4. 删除
注意：一般不建议硬删除，一般是软删除

```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$delete_where = [
    'name' => '第55个应用'
];
$delete_res = $model->delete($delete_where);
var_dump($delete_res);
```

<br>

#### 5. 查询
##### A. 查询一条
getOne 第一个参数（必）：where语句  第二个参数（非必）：字段
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$where = [
    'name' => '第54个应用'
];
$one_app = $model->getOne($where);
var_dump($one_app);

$one_app = $model->getOne($where, ['id', 'description']);
var_dump($one_app);
```

<br>

##### B. 查询所有
getAll 第一个参数（非必）：字段
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$all_apps = $model->getAll(['id', 'description']);
var_dump($all_apps);
```

<br>

##### C. 查询多个
get  第一个参数（必）：where语句  第二个参数（非必）：字段
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$where = [
    'product_id' => 3
];
$apps = $model->get($where, ['id', 'description']);
var_dump($apps);
```

<br>

##### D. 排序
order_by 数组参数，支持多个条件组合排序，正序：asc，倒序：desc
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$where = [
    'product_id' => 3,
    'service_id' => 2
];
$apps = $model->order_by(['id' => 'desc'])->get($where, ['id']);
var_dump($apps);
```

##### E. limit查询
limit查询，查询固定的几条
注意：limit必须在get前面才会生效

```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

// limit，必须在get前面
$where = [
    'product_id' => 3,
    'service_id' => 2
];
$apps = $model->order_by(['id' => 'desc'])->limit(2)->get($where, ['id']);
var_dump($apps);

```
<br>

##### F. 大于/小于查询
* gt：大于查询 >
* lg：小于查询 <
* egt：大于等于查询 >=
* elt：小于等于查询 <=
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$where = [
    'product_id' => 3,
    'status' => ['elt' => 4]
];
$start_time = date('Y-m-d H:i:s', strtotime('-1 day'));
$end_time = date('Y-m-d H:i:s');
$where['created_at']['gt'] = $start_time;
$where['created_at']['lt'] = $end_time;
```

<br>

##### G. 不等于查询
ne 不等于 支持数组也支持字符串
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$where = [
    'product_id' => ['ne' => [1, 2]],
    'status' => ['ne' => 3]
];

$apps = $model->get($where, ['id']);
var_dump($apps);
```

<br>

##### H. like查询
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$where = [
    'name' => ['like' => '%申请%']
];
$apps = $model->get($where, ['id', 'name']);
var_dump($apps);
```

<br>

##### I. is NULL/is not NULL查询
* is_null   is NULL
* is_not_null   is NOT NULL
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$where = [
    'deleted_at' => ['is_null' => '']
];
$apps = $model->get($where, ['id', 'name']);
var_dump($apps);
```

<br>

##### J. in/not in 查询
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$where = [
    'app_type' => ['in' => ['appsvr', 'svr']],
    'product_id' => ['not_in' => [1, 3]]
];
$apps = $model->get($where, ['id', 'name', 'app_type', 'product_id']);
var_dump($apps);
```

<br>

##### K. 查询上次执行的sql
可以通过$model->last_sql来输出上次执行的sql
```php
<?php

// 表名
$table_name = 'phporm.app_info';

// 实例化
$model = PHPOrm::init($table_name);

$sql = $model->last_sql;
var_dump($sql);
```

<br>

### Todo 
* 缓存字段
* 测试Oracle和PostgreSQL
* join方法
* group by方法
* 聚合查询-count、max、min、avg、sum
* 原生sql执行
* debug
* error
* 事务功能