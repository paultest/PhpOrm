<?php
/**
 * 删除数据
 */
require_once '../common.php';

try {
    // 表名
    $table_name = 'phporm.app_info';

    // 实例化
    $model = PHPOrm::init($table_name);

    // 删除
    $delete_where = [
        'name' => '第55个应用'
    ];
    $delete_res = $model->delete($delete_where);
    var_dump($delete_res);

    // 查询上次SQL
    var_dump($model->last_sql);
} catch (\DB_Exception $e) {
    var_dump('数据库错误');
    var_dump($e->getMessage());
} catch (\Exception $e) {
    var_dump('其他错误');
    var_dump($e->getMessage());
}