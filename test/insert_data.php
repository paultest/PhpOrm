<?php
/**
 * 新增数据
 */
require_once '../common.php';

try {
    // 表名
    $table_name = 'phporm.app_info';

    // 实例化
    $model = PHPOrm::init($table_name);

    // 新增一条数据
    $single_data = [
        'name' => '第53个应用',
        'description' => '这个是描述',
        'product_id' => 4,
        'service_id' => 3,
        'developer' => 'hello',
        'operater' => 'xiaoming',
        'ipmortance' => 4,
        'app_type' => 'web',
    ];
    $single_res = $model->insert($single_data);
    var_dump($single_res);

    // 新增多条数据
    $batch_data = [
        [
            'name' => '第54个应用',
            'description' => '这个是描述',
            'product_id' => 4,
            'service_id' => 3,
            'developer' => 'hello',
            'operater' => 'xiaoming',
            'ipmortance' => 4,
            'app_type' => 'web',
        ],
        [
            'name' => '第55个应用',
            'description' => '这个是描述',
            'product_id' => 3,
            'service_id' => 6,
            'developer' => 'hello2',
            'operater' => 'xiaoming55',
            'ipmortance' => 4,
            'app_type' => 'server',
        ],
    ];
    $batch_res = $model->batch_insert($batch_data);
    var_dump($batch_res);

    // 查询上次SQL
    var_dump($model->last_sql);
} catch (\DB_Exception $e) {
    var_dump('数据库错误');
    var_dump($e->getMessage());
} catch (\Exception $e) {
    var_dump('其他错误');
    var_dump($e->getMessage());
}