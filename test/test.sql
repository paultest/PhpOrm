/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.24 : Database - phporm
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`phporm` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `phporm`;

/*Table structure for table `app_info` */

DROP TABLE IF EXISTS `app_info`;

CREATE TABLE `app_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `description` varchar(1000) NOT NULL DEFAULT '' COMMENT '描述',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '一级（产品ID）',
  `service_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '二级（服务ID）',
  `module_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '三级（模块ID）',
  `developer` varchar(100) NOT NULL DEFAULT '' COMMENT '开发负责人',
  `operater` varchar(100) NOT NULL DEFAULT '' COMMENT '运维负责人',
  `ipmortance` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '重要等级 默认0：一般  1:重要  2:核心',
  `boom_url` varchar(100) NOT NULL DEFAULT '' COMMENT 'Boom地址 有BOOM地址为微服务，无则为非微服务架构',
  `app_type` varchar(10) NOT NULL DEFAULT '' COMMENT '业务架构分类， web、appsvr、httpsvr、svr',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态值  0：默认，1：上架，2：在线，3：离线，4：下架，5：异常，6：隔离',
  `file_list_id` varchar(1000) NOT NULL DEFAULT '' COMMENT '文件列表ID',
  `program_list_id` varchar(1000) NOT NULL DEFAULT '' COMMENT '程序列表ID',
  `created_user` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建用户id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_module_id` (`module_id`) USING BTREE,
  KEY `idx_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='CMDB应用信息管理表';

/*Data for the table `app_info` */

insert  into `app_info`(`id`,`name`,`description`,`product_id`,`service_id`,`module_id`,`developer`,`operater`,`ipmortance`,`boom_url`,`app_type`,`status`,`file_list_id`,`program_list_id`,`created_user`,`created_at`,`updated_at`,`deleted_at`) values (1,'za_app_01_new55113','第一个应用的描述,无模55',2,2,0,'admin','test',1,'http://www.baidu.com','web',2,'','',33,'2019-04-18 14:15:04','2020-04-18 13:32:23',NULL),(2,'za_app_02_new','第二个应用的描述,有模块',3,2,1,'admin','test',0,'http://www.baidu.com','web',1,'[1,2,20,21]','0',33,'2019-04-18 17:06:31','2020-04-20 22:35:49',NULL),(3,'za-app-03','第三个应用的描述，存款子业务03',1,2,1,'admin','test',0,'http://www.baidu.com','appsvr',4,'[1,2]','0',33,'2019-04-19 11:06:06','2020-04-20 22:35:49',NULL),(4,'za-app-04','第三个应用的描述，存款子业务02',2,2,1,'admin','test',2,'http://www.baidu.com','web',5,'[1,2]','0',33,'2019-04-19 11:06:45','2020-04-20 22:35:49',NULL),(5,'第五个应用','描述描述',3,2,1,'admin','test',1,'','httpsvr',3,'[1,2]','0',33,'2019-04-23 14:07:05','2020-04-20 22:35:49',NULL),(6,'第六个应用','第六个应用的描述',1,2,1,'admin','test',2,'http://www.baidu.com','web',1,'[1,2]','0',33,'2019-04-23 14:08:20','2020-04-20 22:35:49',NULL),(7,'第七个应用','第七个应用的描述',2,2,1,'admin','test',0,'','web',2,'[1,2]','0',33,'2019-04-23 14:14:17','2020-04-20 22:35:49',NULL),(8,'第八个应用','第八个应用的描述',3,2,1,'admin','test',2,'','httpsvr',3,'[1,2]','0',3,'2019-04-23 16:48:43','2020-04-20 22:35:49',NULL),(9,'za_app_01_new','第一个应用的描述,无模块',1,2,1,'admin','test',2,'http://www.baidu.com','svr',3,'[1,2]','0',3,'2019-04-23 17:18:10','2020-04-20 22:35:49',NULL),(10,'za_app_01_new','第一个应用的描述,无模块',2,2,1,'admin','test',2,'http://www.baidu.com','appsvr',3,'[1,2]','0',3,'2019-04-23 17:26:51','2020-04-20 22:35:49',NULL),(11,'第十一个应用','第十一个应用的描述',3,2,1,'admin','test',1,'http://www.baidu.com','appsvr',2,'[1,2]','0',3,'2019-04-28 19:39:58','2020-04-20 22:35:49',NULL),(12,'第十二个应用','第十二个应用的描述',1,2,1,'admin','test',0,'http://www.baidu.com','web',2,'[1,2]','',3,'2019-04-28 19:45:28','2020-04-20 22:35:49',NULL),(13,'第十三个应用','第十三个应用',2,2,1,'admin','test',2,'http://www.baidu.com','httpsvr',3,'[1,2]','',3,'2019-05-05 15:19:16','2020-04-20 22:35:49',NULL),(14,'第十四个应用','第十四个应用的描述',3,2,1,'admin','test',0,'http://www.baidu.com','web',2,'[1,2]','',3,'2019-05-05 16:41:11','2020-04-20 22:35:49',NULL),(15,'za_app_15_new33','第十五个应用的描述',1,2,1,'admin','test',2,'http://www.baidu.com','svr',2,'[1,2]','',0,'2019-05-06 15:26:15','2020-04-20 22:35:49',NULL),(16,'za_app_16_new33','第一个应用的描述,无模55',2,2,1,'admin','test',0,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-07 15:15:28','2020-04-20 22:35:49',NULL),(17,'za_app_17_new33','第17个应用的描述',3,2,1,'admin','test',2,'http://www.baidu.com','svr',2,'[1,2]','',0,'2019-05-07 15:15:35','2020-04-20 22:35:49',NULL),(18,'za_app_18','第18个应用的描述',1,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-07 19:24:22','2020-04-20 22:35:49',NULL),(19,'za_app_19_new','第19个应用的描述',2,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-07 19:26:00','2020-04-20 22:35:49',NULL),(20,'za_app_20','第20个应用的描述',3,2,1,'admin','test',2,'http://www.baidu.com','svr',0,'[2,10,12,13,15,16]','',0,'2019-05-07 20:35:22','2020-04-20 22:35:49',NULL),(21,'za_app_21_new33','',1,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-09 11:07:28','2020-04-20 22:35:49',NULL),(22,'za_app_22_new33','第22个应用的描述',2,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-09 11:07:44','2020-04-20 22:35:49',NULL),(23,'za_app_23_new33','第23个应用的描述',3,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-09 11:54:48','2020-04-20 22:35:49',NULL),(24,'za_app_24_new33','第24个应用的描述',1,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-09 14:09:58','2020-04-20 22:35:49','2019-05-09 14:10:11'),(25,'第25个应用11','第25个应用的描述',2,2,1,'admin','test',1,'http://www.baidu.com','httpsvr',2,'[1,2]','',0,'2019-05-10 16:42:27','2020-04-20 22:35:49',NULL),(26,'第26个应用','第26个应用',3,2,1,'admin','test',1,'','httpsvr',2,'[1,2]','',3,'2019-05-10 18:00:26','2020-04-20 22:35:49',NULL),(30,'za_app_27_new33','第27个应用的描述',1,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-13 15:05:51','2020-04-20 22:35:49',NULL),(31,'第28个应用','第28个应用',2,2,1,'admin','test',2,'http://www.baidu.com','web',2,'[1,2]','',3,'2019-05-13 15:07:11','2020-04-20 22:35:49',NULL),(32,'第29个应用','第29个应用',3,2,1,'admin','test',2,'http://www.baidu.com','httpsvr',3,'[1,2]','',3,'2019-05-13 15:08:09','2020-04-20 22:35:49',NULL),(35,'za_app_30_new33','第30个应用的描述',3,2,1,'admin','test',2,'http://www.baidu.com','svr',2,'[1,2]','',0,'2019-05-13 15:10:36','2020-04-20 22:35:49',NULL),(36,'za_app_31_new33','第31个应用的描述',1,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-13 15:11:14','2020-04-20 22:35:49',NULL),(38,'za_app_32_new33','第32个应用的描述',3,2,1,'admin','test',2,'http://www.baidu.com','svr',2,'[1,2]','',0,'2019-05-13 16:19:06','2020-04-20 22:35:49',NULL),(39,'za_app_33_new33','第32个应用的描述',1,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',0,'2019-05-13 16:20:45','2020-04-20 22:35:49',NULL),(41,'za_app_35_new33','第35个应用的描述',3,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[2,10,12,13]','[9,11,13,18]',0,'2019-05-13 18:18:13','2020-04-20 22:35:49',NULL),(42,'第36个应用心得','第36个应用new',1,2,1,'admin','test',2,'http://www.baidu.com','svr',2,'[1,2]','',3,'2019-05-13 20:01:48','2020-04-20 22:35:49',NULL),(43,'第37个应用','第37个应用',2,2,1,'admin','test',2,'http://www.baidu.com','web',2,'[1,2]','',3,'2019-05-21 11:58:59','2020-04-20 22:35:49',NULL),(44,'第38个应用','第38个应用的描述',3,2,1,'admin','test',1,'http://www.baidu.com','httpsvr',2,'[1,2]','',3,'2019-05-21 17:04:59','2020-04-20 22:35:49',NULL),(45,'第39个应用','第39个应用的描述',1,2,1,'admin','test',1,'http://www.baidu.com','web',2,'[1,2]','',3,'2019-05-21 20:06:46','2020-04-20 22:35:49',NULL),(46,'第40个应用','第40个应用的描述',2,2,1,'admin','test',2,'http://www.baidu.com','appsvr',2,'[1,2]','',3,'2019-05-22 14:51:44','2020-04-20 22:35:49',NULL),(47,'第41个应用','第41个应用的描述',3,2,1,'admin','test',1,'http://www.baidu.com','appsvr',2,'[22,23]','[19]',3,'2019-05-22 19:48:57','2020-04-20 22:35:49',NULL),(48,'第二个申请.第42个应用','第二个申请-第42个应用的描述',1,2,1,'admin','test',0,'http://www.baidu.com','svr',2,'','',3,'2019-05-27 11:18:51','2020-04-20 22:35:49',NULL),(49,'第七个申请.第43个应用','第七个申请.第43个应用 的描述',2,2,1,'admin','test',2,'http://www.baidu.com','web',2,'','',3,'2019-05-27 11:48:01','2020-04-20 22:35:49',NULL),(50,'zabank-RAS-自定义名1-appsvr','这个是描述，不低于十个字符',3,2,1,'admin','test01',2,'','appsvr',2,'','',2,'2019-06-24 16:32:10','2020-04-20 22:35:49',NULL),(51,'第四个申请','第四个申请的描述',1,2,1,'test','test',1,'http://www.baidu.com','appsvr',2,'','',2,'2019-07-18 21:17:16','2020-04-20 22:35:49',NULL),(52,'第三个申请应用','第三个申请应用的描述\n哈哈哈',2,1,1,'test','test01',2,'http://www.baidu.com','web',2,'','',2,'2019-07-18 21:24:05','2020-04-18 13:32:51',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
