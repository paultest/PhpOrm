<?php
/**
 * 更新数据
 */
require_once '../common.php';

try {
    // 表名
    $table_name = 'phporm.app_info';

    // 实例化
    $model = PHPOrm::init($table_name);

    // 更新
    $update_data = [
        'name' => '第54个应用 - 新的',
        'description' => '这个是描述 - 新的'
    ];
    $update_res = $model->update($update_data, ['id' => 57]);
    var_dump($update_res);

    // 查询上次SQL
    var_dump($model->last_sql);
} catch (\DB_Exception $e) {
    var_dump('数据库错误');
    var_dump($e->getMessage());
} catch (\Exception $e) {
    var_dump('其他错误');
    var_dump($e->getMessage());
}