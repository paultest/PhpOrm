<?php

// 根路径 ROOT_PATH 定义, 5.3以后可以直接使用__DIR__
// 所有被包含文件均须使用此常量确定路径
define('ROOT_PATH', realpath(dirname(__FILE__)) . '/');

// 公共函数
require_once ROOT_PATH . "lib/global.func.php";

// 注册框架默认的__autoload函数
spl_autoload_register('autoload');

// 公共配置文件
require_once ROOT_PATH . 'config/config.php';